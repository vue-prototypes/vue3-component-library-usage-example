import App from './App.vue'
import { createApp } from 'vue'
import plugin from '@ataias/vue3-component-library'

createApp(App)
    .use(plugin)
    .mount('#app')
